<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Api
 */
namespace Eparts\Support\Api;

/**
 * Interface LoginRepositoryInterface
 */
interface LoginRepositoryInterface
{

    /**
     * @param string $login
     * @param string $senha
     * @return mixed
     */
    public function getToken(string $login, string $senha): string;
}
