<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Api
 */
namespace Eparts\Support\Api;

/**
 * Interface OrderRepositoryInterface
 */
interface OrderRepositoryInterface
{

    /**
     * @return mixed
     */
    public function get(): string;

    /**
     * @return mixed
     */
    public function confirm();

    /**
     * @param string $invoiceId
     * @param string $invoiceKey
     * @return mixed
     */
    public function invoice(string $invoiceId, string $invoiceKey);

    /**
     * @param string $trackingId
     * @return mixed
     */
    public function tracking(string $trackingId);

    /**
     * @return mixed
     */
    public function shipping();

    /**
     * @return mixed
     */
    public function cancel();
}
