<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Api
 */
namespace Eparts\Support\Api;

/**
 * Interface ProductRepositoryInterface
 */
interface ProductRepositoryInterface
{

    /**
     * @return mixed
     */
    public function create(): string;
}
