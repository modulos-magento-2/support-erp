<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package  Eparts\Support\Helper
 */

namespace Eparts\Support\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Indexer\Model\Indexer\Collection;
use Magento\Indexer\Model\IndexerFactory;

class Data
{
    /**
     * @var ScopeConfigInterface
     */
    protected $config;

    /**
     * @var IndexerFactory
     */
    protected $indexFactory;

    /**
     * @var Collection
     */
    protected $indexCollection;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $config
     * @param IndexerFactory $indexFactory
     * @param Collection $indexCollection
     */
    public function __construct(
        ScopeConfigInterface $config,
        IndexerFactory $indexFactory,
        Collection $indexCollection
    )
    {
        $this->config = $config;
        $this->indexCollection = $indexCollection;
        $this->indexFactory = $indexFactory;
    }

    /**
     * @param $login
     * @param $pass
     * @return string
     */
    public function getTokenApi($login, $pass): string
    {
        $username = $this->config->getValue('eparts_support/general/username');
        $password = $this->config->getValue('eparts_support/general/password');

        if ($username != $login || $password != $pass) {
            return false;
        }

        return md5($username . ': ' . $password);
    }

    /**
     * Validate token request
     *
     * @param $token
     * @return bool
     */
    public function validateToken($token): bool
    {
        $username = $this->config->getValue('eparts_support/general/username');
        $password = $this->config->getValue('eparts_support/general/password');

        $tokenDb = $this->getTokenApi($username, $password);
        if ($token == $tokenDb) {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getStatusInvoice()
    {
        return $this->config->getValue('eparts_support/fiscalinvoice/status');
    }

    /**
     * @return mixed
     */
    public function getStatusTracking()
    {
        return $this->config->getValue('eparts_support/tracking/status');
    }

    /**
     * @return mixed
     */
    public function getStatusShipping()
    {
        return $this->config->getValue('eparts_support/shipping/status');
    }

    /**
     * @return mixed
     */
    public function getStatusNewProduct()
    {
        return $this->config->getValue('eparts_support/general/product_status');
    }

    public function getSellerId($store)
    {
        return $this->config->getValue('eparts_support/general/seller_id', 'store', $store);
    }

    /**
     * @param $isSuccess
     * @param $msg
     * @param $token
     * @param bool $showToken
     * @return array
     */
    public function getResponse($isSuccess, $msg, $token, bool $showToken = true, $statusCode = false): array
    {
        $return = [
            'sucesso' => $isSuccess,
            'mensagem' => $msg,
            'token' => $token,
            'statusCode' => 200
        ];

        if (!$statusCode) {
            unset($return['statusCode']);
        }

        if (!$showToken) {
            unset($return['token']);
        }

        return $return;
    }

    /**
     * @throws \Exception
     */
    public function manuallIndexing()
    {
        $indexes = $this->indexCollection->getAllIds();
        foreach ($indexes as $index) {
            if (in_array($index, ['cataloginventory_stock', 'catalog_product_price', 'catalog_category_product'])) {
                $indexFactory = $this->indexFactory->create()->load($index);
                $indexFactory->reindexAll($index);
                $indexFactory->reindexRow($index);
            }

        }
    }

    /**
     * @param $string
     * @return array|string|string[]|null
     */
    public static function onlyNumbers($string)
    {
        return preg_replace('/\D/', '', $string);
    }

    /**
     * @param $method
     * @return mixed|null
     */
    public function getPaymentCode($method)
    {
        if ($this->config->getValue('eparts_support/payments/credit_card/method') == $method) {
            return $this->config->getValue('eparts_support/payments/credit_card/code');
        }

        if ($this->config->getValue('eparts_support/payments/billet/method') == $method) {
            return $this->config->getValue('eparts_support/payments/billet/code');
        }

        if ($this->config->getValue('eparts_support/payments/transfer_bank/method') == $method) {
            return $this->config->getValue('eparts_support/payments/transfer_bank/code');
        }

        if ($this->config->getValue('eparts_support/payments/mercadopago/method') == $method) {
            return $this->config->getValue('eparts_support/payments/mercadopago/code');
        }

        if ($this->config->getValue('eparts_support/payments/sinc/method') == $method) {
            return $this->config->getValue('eparts_support/payments/sinc/code');
        }

        return null;
    }
}
