<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Model\Repositories
 */
namespace Eparts\Support\Model\Repositories;

use Eparts\Support\Api\LoginRepositoryInterface;
use Eparts\Support\Helper\Data;

/**
 * Class Login
 */
class Login implements LoginRepositoryInterface
{
    protected $helperData;

    public function __construct(Data $helper)
    {
        $this->helperData = $helper;
    }

    /**
     * @param string $login
     * @param string $senha
     * @return mixed
     */
    public function getToken(string $login, string $senha): string
    {
        $verifyToken = $this->helperData->getTokenApi($login, $senha);
        if (!$verifyToken) {
            return json_encode($this->helperData->getResponse(false, 'Usuário ou senha Inválidos', ''));
        }

        return json_encode($this->helperData->getResponse(true, 'Login OK', $verifyToken));
    }
}
