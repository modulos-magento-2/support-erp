<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Model\Repositories
 */

namespace Eparts\Support\Model\Repositories;

use Eparts\Support\Api\OrderRepositoryInterface;
use Eparts\Support\Helper\Data;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Sales\Model\Order as ModelOrder;
use Magento\Customer\Model\ResourceModel\CustomerRepository;

/**
 * Class Order
 */
class Order implements OrderRepositoryInterface
{
    const PREFIX_COMMENT = '[SUPPORT ERP] - ';
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ModelOrder
     */
    protected $modelOrder;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;


    /**
     * Order constructor.
     * @param Data $helper
     * @param Request $request
     * @param ModelOrder $modelOrder
     * @param CustomerRepository $customerRepository
     */
    public function __construct(
        Data $helper,
        Request $request,
        ModelOrder $modelOrder,
        CustomerRepository $customerRepository
    )
    {
        $this->helperData = $helper;
        $this->request = $request;
        $this->modelOrder = $modelOrder;
        $this->customerRepository = $customerRepository;
    }

    /**
     * return string
     * @return mixed
     */
    public function get(): string
    {
        $orders = [];
        foreach ($this->getOrderList() as $order) {
            $discount = $order->getBaseDiscountAmount();
            $subtotal = $order->getBaseSubtotal();
            $orders[] = [
                'orderId' => $order->getIncrementId(),
                'orderDate' => $order->getCreatedAt(),
                'OrderTotalItens' => number_format($subtotal + $discount, 4, '.', ''),
                'OrderTotal' => $order->getGrandTotal(),
                'OrderTotalFrete' => $order->getShippingAmount(),
                'OrderQtyInstallments' => '1',
                'customer' => $this->getDataCustomer($order),
                'ItensDoPedido' => $this->getItemsOrders($order->getAllItems(), $order->getStoreId()),
                "site" => 'SIM',
                "paymentTypeId" => $this->helperData->getPaymentCode($order->getPayment()->getMethod()),
                "paymentQtyInstallments" => '0',
                "ValorAddTotal" => '0',
                'status' => '002 - Pagamento efetuado'
            ];

        }

        return json_encode($orders);

    }

    /**
     * @return string
     */
    public function confirm(): string
    {
        $incrementId = $this->request->getParam('orderId');
        if (empty($incrementId)) {
            return json_encode($this->helperData->getResponse(false, 'Parâmetro orderID obrigatório', null, false));
        }

        try {
            $order = $this->modelOrder->loadByIncrementId($incrementId);
            $isValidOrder = $this->validateOrder($incrementId);
            if (!is_bool($isValidOrder)) {
                return $isValidOrder;
            }

            $order->setIsIntegratedSupport(1);
            $order->save();
            return json_encode($this->helperData->getResponse(true, 'Confirmação enviada com sucesso.', null, false, 200));
        } catch (\Exception $e) {
            return json_encode($this->helperData->getResponse(false, $e->getMessage(), null, false));
        }
    }

    /**
     * @param string $invoiceId
     * @param string $invoiceKey
     * @return string
     */
    public function invoice(string $invoiceId, string $invoiceKey): string
    {
        $incrementId = $this->request->getParam('orderId');
        try {
            if (empty($incrementId)) {
                return json_encode($this->helperData->getResponse(false, 'Parâmetro orderID obrigatório', null, false));
            }

            $order = $this->modelOrder->loadByIncrementId($incrementId);
            $isValidOrder = $this->validateOrder($incrementId);
            if (!is_bool($isValidOrder)) {
                return $isValidOrder;
            }

            $status = $this->helperData->getStatusInvoice();
            $order->setInvoiceId($invoiceId);
            $order->setInvoiceKey($invoiceKey);
            $order->addCommentToStatusHistory(self::PREFIX_COMMENT . 'Invoice ID: ' . $invoiceId, $status);
            $order->addCommentToStatusHistory(self::PREFIX_COMMENT . 'Invoice KEY: ' . $invoiceKey, $status);

            if ($status) {
                $order->setStatus($status);
            }

            $order->save();
            return json_encode($this->helperData->getResponse(true, 'Registro de invoice enviado com sucesso', null, false));
        } catch (\Exception $e) {
            return json_encode($this->helperData->getResponse(false, $e->getMessage(), null, false));
        }
    }

    /**
     * @param string $trackingId
     * @return bool|string
     */
    public function tracking(string $trackingId)
    {
        $incrementId = $this->request->getParam('orderId');
        try {
            if (empty($incrementId)) {
                return json_encode($this->helperData->getResponse(false, 'Parâmetro orderID obrigatório', null, false));
            }

            $order = $this->modelOrder->loadByIncrementId($incrementId);
            $isValidOrder = $this->validateOrder($incrementId);
            if (!is_bool($isValidOrder)) {
                return $isValidOrder;
            }

            $status = $this->helperData->getStatusTracking();
            $order->setTrackingId($trackingId);
            $order->addCommentToStatusHistory(self::PREFIX_COMMENT . 'Tracking ID: ' . $trackingId, $status);
            if ($status) {
                $order->setStatus($status);
            }
            $order->save();
            return json_encode($this->helperData->getResponse(true, 'Registro de tracking enviado com sucesso', null, false));
        } catch (\Exception $e) {
            return json_encode($this->helperData->getResponse(false, $e->getMessage(), null, false));
        }
    }

    /**
     * @return bool|string
     */
    public function shipping()
    {
        $incrementId = $this->request->getParam('orderId');
        try {
            if (empty($incrementId)) {
                return json_encode($this->helperData->getResponse(false, 'Parâmetro orderID obrigatório', null, false));
            }

            $order = $this->modelOrder->loadByIncrementId($incrementId);
            $isValidOrder = $this->validateOrder($incrementId);
            if (!is_bool($isValidOrder)) {
                return $isValidOrder;
            }

            $status = $this->helperData->getStatusShipping();
            $order->addCommentToStatusHistory(self::PREFIX_COMMENT . 'Confirmação de entrega ');
            if ($status) {
                $order->setStatus($status);
            }
            $order->save();
            return json_encode($this->helperData->getResponse(true, 'Pedido confirmado com sucesso', null, false));
        } catch (\Exception $e) {
            return json_encode($this->helperData->getResponse(false, $e->getMessage(), null, false));
        }
    }

    /**
     * @return bool|string
     */
    public function cancel()
    {
        $incrementId = $this->request->getParam('orderId');
        try {
            if (empty($incrementId)) {
                return json_encode($this->helperData->getResponse(false, 'Parâmetro orderID obrigatório', null, false));
            }

            $order = $this->modelOrder->loadByIncrementId($incrementId);
            $isValidOrder = $this->validateOrder($incrementId);
            if (!is_bool($isValidOrder)) {
                return $isValidOrder;
            }

            $status = $this->helperData->getStatusShipping();
            $order->addCommentToStatusHistory(self::PREFIX_COMMENT . 'Envio de cancelamento ');
            $order->setStatus('canceled');
            $order->setState('canceled');
            $order->cancel();
            $order->save();
            return json_encode($this->helperData->getResponse(true, 'Pedido cancelado com sucesso', null, false));
        } catch (\Exception $e) {
            return json_encode($this->helperData->getResponse(false, $e->getMessage(), null, false));
        }
    }

    /**
     * @param $id
     * @return bool|string
     */
    protected function validateOrder($id)
    {
        $order = $this->modelOrder->loadByIncrementId($id);
        if (empty($order->getEntityId())) {
            return json_encode($this->helperData->getResponse(false, 'Pedido não encontrado.', null, false));
        }

        return true;
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     */
    protected function getOrderList()
    {
        return $this->modelOrder->getCollection()
            ->addFieldToFilter('is_integrated_support', ['null' => true]);
    }

    /**
     * @param $order
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function getDataCustomer($order): array
    {
        $customer = $this->customerRepository->getById($order->getCustomerId());
        $address = $order->getBillingAddress();
        return [
            'id' => $order->getCustomerId(),
            'legalDocument1' => preg_replace('/\D/', '', $customer->getTaxvat()),
            'name' => $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname(),
            'OrderEmail' => $order->getCustomerEmail(),
            'adressStreet' => $address->getStreetLine(1),
            'addressNumber' => $address->getStreetLine(2),
            'addressCountry' => 'BR',
            'addressCity' => $address->getCity(),
            'addressState' => $address->getRegionCode(),
            'addressZipCode' => $address->getPostcode(),
            'phonesPhoneDDD' => substr($this->helperData::onlyNumbers($address->getTelephone()), 0, 2),
            'phonesPhoneNumber' => substr($this->helperData::onlyNumbers($address->getTelephone()), 2),
            'genre' => $this->getGender($customer->getGender()),
            'birthDate' => date('Y-d-m', strtotime($customer->getDob())),
        ];
    }

    /**
     * @param $items
     * @param $storeId
     * @return array
     */
    protected function getItemsOrders($items, $storeId): array
    {
        $orderItems = [];
        foreach ($items as $item) {
            $orderItems[] = [
                'SellerId' => $this->helperData->getSellerId($storeId),
                'sellerSKU' => $item->getSku(),
                'NomeProduto' => $item->getName(),
                'Preco_Unitario' => $item->getBasePrice(),
                'Quantidade' => $item->getQtyOrdered(),
                'percentualDesconto' => $item->getDiscountPercent(),
                'DescontoCupom' => $item->getDiscountAmount(),
                'Ean' => 0
            ];
        }

        return $orderItems;
    }

    /**
     * @param $gender
     * @return string
     */
    protected function getGender($gender): string
    {
        switch ($gender) {
            case 1:
                return 'M';
            case 2:
                return 'F';
            default:
                return 'N/A';
        }
    }

    /**
     * @param $price
     * @param $discount
     * @return float|int|string
     */
    protected function calculateDiscount($price, $discount)
    {
        if (empty($discount)) {
            return '0.0000';
        }

        $discountPerc = ($discount / $price) * 100;
        return $discountPerc * 1;
    }
}
