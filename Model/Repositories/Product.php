<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Model\Repositories
 */

namespace Eparts\Support\Model\Repositories;

use Eparts\Support\Api\ProductRepositoryInterface;
use Eparts\Support\Helper\Data;
use Laminas\Json\Json;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Catalog\Model\Product as ModelProduct;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

/**
 * Class Product
 */
class Product implements ProductRepositoryInterface
{
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var StockStateInterface
     */
    protected $_stockStateInterface;

    /**
     * @var StockRegistryInterface
     */
    protected $_stockRegistry;

    /**
     * @var ModelProduct
     */
    protected $modelProduct;

    /**
     * Product constructor.
     * @param Data $helper
     * @param Request $request
     * @param ModelProduct $modelProduct
     * @param StockRegistryInterface $stockRegistry
     * @param StockStateInterface $stockState
     */
    public function __construct(
        Data $helper,
        Request $request,
        ModelProduct $modelProduct,
        StockRegistryInterface $stockRegistry,
        StockStateInterface $stockState

    )
    {
        $this->helperData = $helper;
        $this->request = $request;
        $this->modelProduct = $modelProduct;
        $this->_stockRegistry = $stockRegistry;
        $this->_stockStateInterface = $stockState;
    }

    /**
     * return string
     * @return string
     */
    public function create(): string
    {
        $params = $this->getData();
        try {
            $idProduct = $this->verifyProductExists($params['sellerSKU']);
            if (!$idProduct) {
                $this->createProduct($params);
            } else {
                $this->updateProduct($idProduct, $params);
            }
            return json_encode($this->helperData->getResponse(true, 'Produto enviado com sucesso.', null, false));
        } catch (\Exception $e) {
            return json_encode($this->helperData->getResponse(false, $e->getMessage(), null, false));
        }
    }

    /**
     * @return array
     */
    protected function getData(): array
    {
        return $this->request->getBodyParams();
    }

    /**
     * @param $sku
     * @return int
     */
    protected function verifyProductExists($sku): int
    {
        return intval($this->modelProduct->getIdBySku($sku));
    }

    /**
     * @param $data
     * @throws \Exception
     */
    protected function createProduct($data)
    {
        $statusConfig = $this->helperData->getStatusNewProduct();
        $status = !empty($data['status']) ? 1 : 2;
        if ($statusConfig) {
            $status = 1;
        }

        try {
            $product = $this->modelProduct;
            $product->setName($data['name']);
            $product->setAttributeSetId(4);
            $product->setTypeId('simple');
            $product->setWebsiteIds([1]);
            $product->setSku($data['sellerSKU']);
            $product->setDescription($data['description']);
            $product->setShortDescription($data['description']);
            $product->setVisibility(4);
            $product->setTaxClassId(0);
            $product->setPrice($data['price']);

            #set special price
            if ($data['priceDiscount'] < $data['price']) {
                $product->setSpecialPrice($data['priceDiscount']);
            }

            $product->setStatus($status);
            $product->setWeight($data['weight']);
            $product->setVolumeHeight($data['height']);
            $product->setVolumeLength($data['length']);
            $product->setVolumeWidth($data['width']);
            $stockData = [
                'manage_stock' => 1,
                'is_in_stock' => ($data['quantity'] > 0) ? 1 : 0,
                'qty' => $data['quantity']

            ];
            $product->setStockData($stockData);
            $product->setQuantityAndStockStatus($stockData);
            $product->save();
            $this->helperData->manuallIndexing();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $idProduct
     * @param $data
     * @throws \Exception
     */
    protected function updateProduct($idProduct, $data)
    {
        $stockData = [
            'manage_stock' => 1,
            'is_in_stock' => ($data['quantity'] > 0) ? 1 : 0,
            'qty' => $data['quantity']

        ];

        try {

            $product = $this->modelProduct->load($idProduct);
            $product->setPrice($data['price']);

            #set special price
            if ($data['priceDiscount'] < $data['price']) {
                $product->setSpecialPrice($data['priceDiscount']);
            }
            $product->setStockData($stockData);
            $product->setQuantityAndStockStatus($stockData);
            $product->save();
            $this->helperData->manuallIndexing();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}
