<?php

namespace Eparts\Support\Model\Source;

use  Magento\Payment\Helper\Data;
use Magento\Framework\Option\ArrayInterface;

/**
 * Class Payments
 * @package Eparts\Support\Model\Source
 */
class Payments implements ArrayInterface
{
    /**
     * @var Data
     */
    protected $helperPayment;

    /**
     * Payments constructor.
     * @param Data $helperPayment
     */
    public function __construct(Data $helperPayment)
    {
        $this->helperPayment = $helperPayment;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $payment[] = [
            'label' => '-- Selecione uma opção --',
            'value' => ''
        ];

        $payments = $this->helperPayment->getPaymentMethods();
        foreach ($payments as $code => $data) {
            if (isset($data['active']) && $data['active'] == '1') {
                $payment[] = [
                    'label' => $data['title'],
                    'value' => $code
                ];
            }
        }

        return $payment;
    }


}
