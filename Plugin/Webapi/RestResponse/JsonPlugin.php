<?php

/**
 * @author Diego Santos<diegossantos05@gmail.com>
 * @copyright Copyright (c) 2021 WebParts
 * @package Eparts\Support\Plugin\Webapi\RestResponse
 */

namespace Eparts\Support\Plugin\Webapi\RestResponse;

use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Webapi\Rest\Response\Renderer\Json;

/**
 * Class JsonPlugin
 */
class JsonPlugin
{
    /** @var Request */
    protected $request;

    /**
     * JsonPlugin constructor.
     * @param Request $request
     */
    public function __construct(
        Request $request
    )
    {
        $this->request = $request;
    }

    /**
     * @param Json $jsonRenderer
     * @param callable $proceed
     * @param $data
     * @return mixed
     */
    public function aroundRender(Json $jsonRenderer, callable $proceed, $data)
    {
        if ($this->isJson($data) && $this->getUrlConvert($this->request->getPathInfo())) {
            return $data;
        }

        return $proceed($data);
    }

    /**
     * @param $data
     * @return bool
     */
    protected function isJson($data): bool
    {
        if (!is_string($data)) {
            return false;
        }
        json_decode($data);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param $url
     * @return bool
     */
    protected function getUrlConvert($url): bool
    {

        $arr = [
            '/V1/supervisor/ws/login',
            '/V1/seller/catalog/offers',
            'V1/fulfillment/order'
        ];

        if ($this->validateUrl($url, $arr)) {
            return true;
        }

        return false;
    }

    /**
     * @param $haystack
     * @param array $needles
     * @param int $offset
     * @return false
     */
    protected function validateUrl($haystack, array $needles = array(), int $offset = 0): bool
    {
        $chr = [];
        foreach ($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) {
                $chr[$needle] = $res;
            }
        }

        if (empty($chr)) {
            return false;
        }

        return true;

    }
}
